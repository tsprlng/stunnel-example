#!/usr/bin/zsh -e

# This script is usable as a ProxyCommand option to SSH for a (relatively) self-contained solution.

do_it() {
	(ARGV0=ssh-stunnel exec stunnel -fd 3 3<&0 0<&4) <<-EOFCFG
		foreground = yes
		client = yes
		connect = $1:443
		ciphers = PSK
		PSKsecrets = $2
		sni = ssh.stunnel
	EOFCFG
}

ip='???.???.??.???'  # The IP of your stealthy server

exec 4<&0  # Pass stdin into the stunnel process via FD 4 (so we can use a heredoc for config).
do_it "$ip" =(echo pskkey:psksecret)  # Saves having to have a separate PSK file
